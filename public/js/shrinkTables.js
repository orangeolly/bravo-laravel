$(document).ready(function(){
	$('table tr').hide();
	$('table').each(function(){
		$(this).find('tr:first').show().addClass("topline");;
		$(this).find('tr:first').click(function(){
			table = $(this).closest('table');
			table.find('tr:first').toggle()
			table.find('tr').toggle();
			table.find('tr:first').show();
		});
		$(this).hover(function() {
		    $(this).css('cursor','pointer');
		}, function() {
		    $(this).css('cursor','auto');
		});

	});
})
