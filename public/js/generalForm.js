function checkTextAreaLength(area,maxChars)
{
	if (area.val().length>maxChars) 
	{
		$(".formSubmit").prop( "disabled", true );
		$("#tooBigWarning").show();
		return false;
	}
	else
		{
		$(".formSubmit").prop( "disabled", false );
		$("#tooBigWarning").hide();
		return true;
		}	
}

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

/// Clearly requires the email field to have the id="email"
/// the label for the email field to be called titleEmail
/// and a message box called emailMessage.
function checkEmail()
{
	if ($("#email").val().length==0) return false;
	if (validateEmail($("#email").val()) || $("#email").val()=="" )
	{
		
		$("#titleEmail").removeClass("warn");
		$("#emailMessage").html("<br>");
		return true;
	}
	else
	{
		$("#titleEmail").addClass("warn");
		$("#emailMessage").text("Email is not valid");
		return false;
	}	
}