/// global variables.
var GLOB_VAR1;

function exampleAjaxCall(){
	console.log("exampleAjaxCall() intelrot entered");
	var dataValueVariable = 1;
  	 $.post(	"./ajax.php?commandName=whatYouWantedToHappen",
  		{
			globVar1: GLOB_VAR1,
			datafieldName: "dataValue",
			datafieldName2: dataValueVariable
  		},
  		function(data,status)				  			
  		{
  			console.log("got this data back"+data);
  			data = checkForPhpErrorsWhileParsingJson(data);
  			/// do stuff you want to do now the result is back.
  		}

  	)
}

function checkForPhpErrorsWhileParsingJson(data)
{
	/// uncomment the line below to test the error handling.
	// data = "fake error"+data;

    try {
        data = JSON.parse(data);
    } catch(e) {
    	/// TODO alert the server
        //alert(e); // error in the above string (in this case, yes)!
        console.log("Json parsing error:"+e);
        console.log("Raw data to parse was:"+data);

  		var err = {};
  		err['parseError']=e.message;
  		err['stack']=e.stack;
  		err['dataToParseWas']=data;
  		err['title']="A data transfer did not complete successfully, possibly because the internet connection is intermittent";
  		submitErrors(err);


        return false;
    }
  	//data = JSON.parse(data);	
  	//console.log(data);
  	return data;
}

/// a trick to toggle a section of screen with ctrl z
function KeyPress(e) 
{ // detect ctrl z
  var evtobj = window.event? event : e
  if (evtobj.keyCode == 90 && evtobj.ctrlKey) $(".secretData").toggle();;
}



$(document).ready(function() 
{
	GLOB_VAR1 = 3;
	exampleAjaxCall();
});
