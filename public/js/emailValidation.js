$(document).ready(function(){
  
  	function checkAll(){
	  if (checkEmail() && checkTextAreaLength($("#message"),2000))
	  {
	    $(".formSubmit").prop( "disabled", false );
	  }
	  else
	  {
	    $(".formSubmit").prop( "disabled", true );
	  }
	}
	
	checkAll();
	$("#tooBigWarning").hide();
	$("#message").keyup(function(){
		checkAll();
	});
	$("#message").click(function(){
		checkAll();
	});
	
	$("#email").keyup(function(){
		checkAll();
	});	
	$("#email").click(function() {
		checkAll();
	});
	

});