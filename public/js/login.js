function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

$(document).ready(function() 
{
	$("#forgotten").prop("disabled",true);
	$("#username").keyup(function(){
		if ($(this).val().length>2)
			{
			 $("#forgotten").prop("disabled",false);
			}
		else
			{
			$("#forgotten").prop("disabled",true);
			}
	});

	/// this is a workaround the fact that Chrome ignores autofill off and does not autofill immediately.
	async function checkUsername() {
	  console.log('wait for chrome');
	  await sleep(200);
	  console.log('its had time to think now.');
	  $("#username").keyup();
	}
	
	$('#forgotten').click(function(){
	  	$("#username2").val($("#username").val());
	});
	
	checkUsername();
	
})
