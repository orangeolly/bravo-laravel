<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Route::post('/home', 'HomeController@index');

Route::get('/home', 'HomeController@index');

Route::get('/uae', function () {
    return view('uae');
});
Route::get('/training', function () {
    return view('training');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/team', function () {
    return view('team');
});
Route::get('/governance', function () {
    return view('governance');
});
Route::get('/mission', 'MissionController@index');

Route::get('/planning', function () {
    return view('planning');
});
Route::get('/assurance', function () {
    return view('assurance');
});

