<script src="./js/navigation.js?v1" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./css/navigation.css">
<meta name="viewport" content="width=device-width, initial-scale=1">


<div id="outer">
    <div id = "top" class="w3-food-cranberry w3-top w3-center" >
        <br>
        <b>BRAVO NOVEMBER Medical consultancy.</b>
        <br>
        <ul id="underlinedList" class="w3-navbar w3-card-2 w3-food-cranberry w3-large">
            <li id="menuToHide"><a  title="toggle menu" class="w3-food-cranberry">Menu</a></li>
            <li class = "menuToToggle"><a href="./home" title="home" class="w3-food-cranberry">logo</a></li>

            <!-- Wide setup -->
            <span id="wide">
  	<li class="w3-right menuToToggle"><a href="./contact">Contact us</a></li>

	  <li class="w3-dropdown-hover w3-right menuToToggle">
		    <a href="javascript:void(0)">Projects</a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	    <a  href="./uae">UAE </a>
	    </div>
	  </li>

	  <li class="w3-dropdown-hover w3-right menuToToggle">
	  	 <a href="javascript:void(0)">Services</a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	    <a  href="./training">Training </a>
	    <a  href="./governance">Governance </a>
	    <a  href="./assurance">Assurance </a>
	    <a  href="./planning">Planning and consultancy </a>
	    </div>
	  </li>

	  <li class="w3-dropdown-hover w3-right menuToToggle">
		    <a href="javascript:void(0)">About us</a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	    <a  href="./mission">Mission Statement and Ethos </a>
	    <a  href="./team">Team </a>
	    </div>
	  </li>

  	<li class="w3-right menuToToggle"><a href="./home">Home</a></li>

</span>
            <!-- Narrow setup -->
            <span id="narrow">
  	<li class="w-right menuToToggle"><a href="./home">Home</a></li>
	  <li class="w3-dropdown-hover w-right menuToToggle">
		    <a href="javascript:void(0)">About us</a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	    <a  href="./mission">Mission Statement and Ethos </a>
	    <a  href="./team">Team </a>
	    </div>



	  <li class="w3-dropdown-hover w-right menuToToggle">
	  	 <a href="javascript:void(0)">Services</a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	    <a  href="./training">Training </a>
	    <a  href="./governance">Governance </a>
	    <a  href="./assurance">Assurance </a>
	    <a  href="./planning">Planning and consultancy </a>
	    </div>
	  </li>

	  <li class="w3-dropdown-hover w-right menuToToggle">
		    <a href="javascript:void(0)">Projects</a>
	    <div class="w3-dropdown-content w3-white w3-card-4">
	    <a  href="./uae">UAE </a>
	    </div>
	  </li>

                </li>
                <li class="w3-right menuToToggle"><a href="./contact">Contact us</a></li>
</span>
        </ul>
    </div>
</div>
    <!--br><br><br><br><br><br><br><br--> <!-- to make errors show-->