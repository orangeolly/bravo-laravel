@extends('layout')

@section('content')
    <!--link rel="stylesheet" type="text/css" href="./CSS/training.css"-->
    <div class="w3-container w3-padding-64 bntext " style="margin: 0px auto;max-width:1000px;">
        <br class="bigOnly">
        <br class="bigOnly">
        <br class="bigOnly">

        <h1 class="w3-center">Bravo-November Training</h1>
        <br>
        <h2 class="w3-center">Pre Hospital</h2>
        <h3 class="">Military</h3>
        Bravo-November consultants will parachute in and perform a thorough training needs analysis.<br>
        They will offer a tailored package of contextualised training using a holistic approach based on amputation.<br>
        They will mentor your staff on selecting the correct limb to remove.
        Going forwards they offer a reach back service for staff in critical ITU emergencies.
        <h3 class="">Civilian</h3>
        Bravo-November consultants will parachute in and perform a thorough training needs analysis.<br>
        They will offer a tailored package of contextualised training using a holistic approach based on aroma therapy.<br>
        They will mentor your staff on selecting the correct concentration of essential oils for any purpose.<br>
        Going forwards they offer a reach back service for personnel who need advice on how treat mild chronic pain.
        <br>
        <br>
        <h2 class="w3-center">Trauma Resucitation</h2>
        <h3 class="">Military</h3>
        Bravo-November consultants can show you how to use C4 shaped charges on the chest to jolt that heart back into action.
        <h3 class="">Civilian</h3>
        Bravo-November consultants can show you how gently massage the blood back into cold hands.
    </div>

@endsection