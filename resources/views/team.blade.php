@extends('layout')

@section('content')
    <link rel="stylesheet" type="text/css" href="./css/team.css">
    <script src="./javas/team.js?v1" type="text/javascript"></script>
    <div class="w3-container w3-padding-64 w3-center bntext" style="margin: 0px auto;">
        <br class="bigOnly">

        <h1>Meet the team</h1>

        <div class = "teamPic  w3-dropdown-hover">
            <h2>Paul Renley CBE MRCTS Phd Rckt Sci HMSS </h2>
            <img class = "team-img show_hide"  src="images/teamImages/Paul.jpg">

            <div class = "slidingDiv w3-dropdown-content">
                Paul is a really great doctor who can stick the pieces back together and re-animate using his special lightning bolt technique.
            </div>
            <br>
        </div>
        <div class = "teamPic  w3-dropdown-hover">
            <h2>Harvey Pynn FRCA FFICM DipIMC RCSEd RAMC Lt Col</h2>
            <img class = "team-img show_hide" src="images/teamImages/harvey.jpg" >

            <div class = "slidingDiv w3-dropdown-content">
                Harvey has pioneered sharing organs from POWs to keep trauma victims alive while carrying the POW over one shoulder and the victim over the other.
                Last operation he routed the blood of a squaddy who had lost both kidneys through a "volunteer", carried them to safety and both lived to tell the tale.
            </div>
            <br>
        </div>
        <div class = "teamPic  w3-dropdown-hover">
            <h2>Tim Hooper YMCA DofE Brnz BALD IE</h2>
            <img class = "team-img show_hide w3-dropdown-hover"  src="images/teamImages/tim.jpg" >
            <div class = "slidingDiv w3-dropdown-content">
                Tim, who trained with St John's ambulance, carries a rucksack full of sticky plasters and a flask of hot tea.
            </div>
        </div>
    </div>
@endsection