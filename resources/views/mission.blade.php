@extends('layout')

@section('content')
    <link rel="stylesheet" type="text/css" href="./css/slideshow.css">
    <link rel="stylesheet" type="text/css" href="./css/mission.css">
    <script src="./js/slideshow.js" type="text/javascript"></script>
    <br class="bigOnly">
    <br class="bigOnly">
    <br class="bigOnly">
    <!--div id="templateBannerMission" class="w3-title" >
      <div  class=" w3-xlarge " style="cursor:default;" title="">Company Mission Statement and Ethos<br>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br>
      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br>
      Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div-->

    <div id="templateBannerMission" class="w3-title bntext" >
        <div  class="w3-animate-opacity  " style="cursor:default;" title="">Medical Governance, Assurance + Training<br>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br>
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br>
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
    </div>

    </div>
    @foreach($pics as $pic)
    <img class = "slide-img" src="images/MI2/{{$pic}}">
    @endforeach
    </div>

@endsection