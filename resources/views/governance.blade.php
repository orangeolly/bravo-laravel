@extends('layout')

@section('content')
    <link rel="stylesheet" type="text/css" href="./css/about.css">
    <div class="w3-container w3-padding-64 bntext" style="margin: 0px auto;max-width:1000px;">
        <br class="bigOnly">
        <br class="bigOnly">
        <br class="bigOnly">

        <h1 class="w3-center">Governance page</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquam faucibus purus in massa tempor nec feugiat nisl pretium. Quam pellentesque nec nam aliquam sem et tortor. Mauris rhoncus aenean vel elit scelerisque mauris. Eros in cursus turpis massa tincidunt dui ut ornare lectus. Ut diam quam nulla porttitor massa id. Maecenas volutpat blandit aliquam etiam erat. Sit amet facilisis magna etiam tempor orci eu lobortis elementum. Ipsum suspendisse ultrices gravida dictum fusce ut. Ut eu sem integer vitae justo. Amet nulla facilisi morbi tempus. Placerat duis ultricies lacus sed turpis tincidunt. Diam phasellus vestibulum lorem sed risus ultricies tristique. Proin sed libero enim sed faucibus turpis in eu. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus. Eu non diam phasellus vestibulum lorem sed risus ultricies tristique. Platea dictumst quisque sagittis purus sit amet volutpat consequat.
        </p>
        Mattis molestie a iaculis at erat pellentesque adipiscing commodo. Natoque penatibus et magnis dis parturient montes nascetur ridiculus. Urna porttitor rhoncus dolor purus non enim praesent elementum. Metus aliquam eleifend mi in nulla posuere sollicitudin. Nunc scelerisque viverra mauris in aliquam sem fringilla. Venenatis tellus in metus vulputate eu. Lacus vel facilisis volutpat est velit egestas dui id. Nibh cras pulvinar mattis nunc sed blandit libero volutpat. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget gravida. Enim neque volutpat ac tincidunt vitae semper quis. Et malesuada fames ac turpis egestas sed. Netus et malesuada fames ac. In mollis nunc sed id semper risus in hendrerit gravida.
        <p>
            Dui accumsan sit amet nulla facilisi morbi tempus iaculis. Malesuada proin libero nunc consequat interdum varius sit. Sed vulputate mi sit amet mauris commodo. Nec ultrices dui sapien eget mi. In nulla posuere sollicitudin aliquam ultrices sagittis. Interdum varius sit amet mattis vulputate enim nulla. Bibendum neque egestas congue quisque. Amet venenatis urna cursus eget nunc. Pharetra magna ac placerat vestibulum lectus. Tincidunt augue interdum velit euismod. Diam ut venenatis tellus in metus vulputate eu scelerisque felis. Sit amet mattis vulputate enim. Pretium vulputate sapien nec sagittis. Netus et malesuada fames ac turpis egestas maecenas pharetra convallis.
        <p>
            Convallis aenean et tortor at risus viverra adipiscing. Eget nunc scelerisque viverra mauris in. Tempor id eu nisl nunc mi ipsum faucibus vitae aliquet. Pellentesque id nibh tortor id aliquet lectus. Ullamcorper velit sed ullamcorper morbi tincidunt. Risus nullam eget felis eget nunc lobortis. Convallis tellus id interdum velit laoreet id donec ultrices. Leo integer malesuada nunc vel risus commodo viverra maecenas accumsan. Vel quam elementum pulvinar etiam non quam. Sem et tortor consequat id porta nibh venenatis cras sed. Massa sapien faucibus et molestie ac feugiat sed. Pharetra magna ac placerat vestibulum lectus mauris ultrices. Aliquet sagittis id consectetur purus ut faucibus pulvinar.
        <p>
            Eget mi proin sed libero enim sed faucibus turpis in. Turpis egestas pretium aenean pharetra magna ac placerat vestibulum. At ultrices mi tempus imperdiet nulla. In iaculis nunc sed augue. Consequat semper viverra nam libero justo laoreet. Quis vel eros donec ac. Diam maecenas sed enim ut sem viverra aliquet eget. Purus faucibus ornare suspendisse sed nisi. Molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit sed. Dapibus ultrices in iaculis nunc sed. Enim nunc faucibus a pellentesque sit amet porttitor. Aliquam eleifend mi in nulla posuere. Quisque id diam vel quam. Non pulvinar neque laoreet suspendisse interdum consectetur libero.
    </div>

@endsection