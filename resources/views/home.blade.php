@extends('layout')

@section('content')

<link rel="stylesheet" type="text/css" href="./css/home.css">
<link rel="stylesheet" type="text/css" href="./css/slideshow.css">
<script src="./js/slideshow.js" type="text/javascript"></script>
<img class = "slide-img" src="images/timsplint.jpg">
<img class = "slide-img"  src="images/fieldHosp.jpg">
<div id="templateBannerHome" class="w3-title" >
    <div  class="w3-animate-opacity w3-food-cranberry bannerMessage" style="cursor:default;" title="">Medical Governance, Assurance +<br>
        Training For Any Environment</div>
    @if ($message)
        <div  class="w3-animate-opacity w3-food-cranberry bannerMessage" style="cursor:default;" title=""><I>{{$message}}</I> </div>
    @endif
</div>
@endsection