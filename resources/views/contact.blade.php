@extends('layout')

@section('content')
<link rel="stylesheet" type="text/css" href="./css/contact.css">
<script src="./js/generalForm.js" type="text/javascript"></script>
<script src="./js/emailValidation.js" type="text/javascript"></script>
<br><br>
<div class="w3-container w3-padding-64 w3-center bntext">
    <h1>Want to talk to us?<br>Email us here and we will get in contact with you</h1>
    <h2>We hate spam and will never share your details or send you un-solicited email</h2>
    <br> <br> <br>
    <form style="" method="post" action="./index.php">
        <input type="hidden" name="command" value="sendEmail">
        Your Name<br> <input
                type="text" name="contactName"> <br>
        <div>
            <label class="desc" id="titleEmail" for="contactEmail" >
                Contact email
            </label>
            <div>
                <input id="email" name="contactEmail" type="email" spellcheck="false" value="" maxlength="255" spellcheck="false">
                <div id="emailMessage" class="warn"><br></div>
            </div>
        </div>
        <br>
        Message <i>(include contact numbers if necessary)</i><BR> <textarea id="message"
                                                                            name="message"></textarea><br> <br> <Br>
        <input  id="emailSubmit" type="submit" class="w3-btn w3-theme formSubmit" value="Send"><br>
        <span id="tooBigWarning"> You have reached the 2000 Character limit for messages </span>
    </form>
</div>
@endsection