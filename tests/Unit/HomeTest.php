<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_Can_See_Message_Posted_to_homepage()
    {
        $message = "hi";
        $this->post('/home',['message'=>$message])
            ->assertSee($message);
    }
}
