<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MissionController extends Controller
{
    public function index()
    {
        $picArray = scandir ( "./images/MI2/" );
        /// discard first 2 . and ..
        $pics = array_slice($picArray,2);
        //dd($usablePics);
        return view('mission')->with('pics',$pics);
    }
}
